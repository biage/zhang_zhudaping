const API = require("../../config/api.js");
const Utils = require("../../utils/utils.js");
const detailConfig = {
  data:{
    detail:{},
    Base: API.ImgBase// 图片前缀
  },
  onLoad(options){
    // 此详情页接口为模拟接口
    Utils.Request(`${API.Details}`, { id: options.id }).then((res) => {
      if (res.statusCode === 200){
        res.data[0].imgs = res.data[0].imgs?res.data[0].imgs.split(','):[];
        this.setData({ detail: res.data[0] });
      }
    });
  }
}
Page(detailConfig);