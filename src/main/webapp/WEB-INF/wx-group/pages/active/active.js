const API = require("../../config/api.js");
const Utils = require("../../utils/utils.js");
const activeConfig = {
  data: {
    list: [],
    isAll: false,
    pageNum: 1,
    totalPage: 0,
    Base: API.ImgBase
  },
  toAdd() {wx.navigateTo({ url: `add/add` });},
  toDetail(ev) {wx.navigateTo({ url: `../detail/detail?id=${ev.currentTarget.dataset.id}` });},
  onShow() {this.getData();},
  onPullDownRefresh(){this.getData();},
  getData() {
    Utils.Request(`${API.ActiveList}`, { bid: wx.getStorageSync('dept') }).then((res) => {
      if (res.statusCode === 200) {
        wx.stopPullDownRefresh();
        let list = res.data.rows;
        for (let i = 0, len = list.length; i < len; i++) {
          list[i].imgs = list[i].imgs ? list[i].imgs.split(',') : [];
        }
        this.setData({ list: list });
        this.setData({ totalPage: res.data.total });
        this.setData({ isAll: res.data.total <= 1 });
      }
    });
  },
  onReachBottom() {
    let pages = this.data.pageNum;
    if (++pages <= this.data.totalPage) {
      wx.showLoading({ title: '加载中...' });
      this.setData({ pageNum: pages });
      Utils.Request(`${API.IndexList}`, { bid: wx.getStorageSync('dept'), page: pages }).then((res) => {
        if (res.statusCode === 200) {
          let list = res.data.rows;
          for (let i = 0, len = list.length; i < len; i++) {
            list[i].imgs = list[i].imgs ? list[i].imgs.split(',') : [];
          }
          this.setData({ list: this.data.list.concat(list) });
          wx.hideLoading();
        }
      });
    } else this.setData({ isAll: true });
  }
}
Page(activeConfig);