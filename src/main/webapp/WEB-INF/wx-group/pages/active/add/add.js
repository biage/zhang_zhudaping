const API = require("../../../config/api.js");
const Utils = require("../../../utils/utils.js");
const addConfig = {
  data: {
    mId: '',
    date: '',
    hostman: '',
    recordman: '',
    hostAuto: [],
    recordAuto: [],
    imgList: [],
    imgNames: [],
    absentList: [{ name: '', reason: '' }],
    absentAuto: [],
    absentIndex: 0,
    isClick: true
  },
  onLoad() {
    this.getDate();
    Utils.Request(`${API.GetMID}`).then((res) => {
      res.statusCode === 200 && this.setData({ mId: res.data.mid });
    });
  },
  // 主持人输入
  hostmanInp(ev) { this.manInp(ev.detail.value, 'host'); },
  // 自动填充设置主持人
  setHostman(ev) { this.setMan(ev.currentTarget.dataset.name, 'host'); },
  // 记录人输入
  recordmanInp(ev) { this.manInp(ev.detail.value, 'record'); },
  // 自动填充设置记录人
  setRecordman(ev) { this.setMan(ev.currentTarget.dataset.name, 'record'); },
  // 缺席人员输入
  absentInp(ev) {
    this.data.absentList[ev.currentTarget.dataset.index].name = ev.detail.value;
    this.setData({ absentIndex: ev.currentTarget.dataset.index });
    this.setData({ absentList: this.data.absentList });
    this.manInp(ev.detail.value, 'absent');
  },
  // 设置缺席人员
  setAbsent(ev) {
    this.data.absentList[ev.currentTarget.dataset.index].name = ev.currentTarget.dataset.name;
    this.setData({ absentList: this.data.absentList, absentAuto: [] });
  },
  // 添加缺席人员
  addAbsent() {
    this.data.absentList.push({ name: '', reason: '' });
    this.setData({ absentList: this.data.absentList, absentAuto: [] });
  },
  // 删除缺席人员
  delAbsent(ev) {
    this.data.absentList.splice(ev.currentTarget.dataset.index, 1);
    this.setData({ absentList: this.data.absentList, absentAuto: [] });
  },
  // 设置对应的缺席原因
  setReason(ev) {
    this.data.absentList[ev.currentTarget.dataset.index].reason = ev.detail.value;
    this.setData({ absentList: this.data.absentList });
  },
  // 自动填充姓名输入
  manInp(val, inp) {
    if (val !== '') {
      Utils.Request(`${API.AutoFill}`, { name: val, dept: wx.getStorageSync('dept') }).then((res) => {
        if (res.statusCode === 200) {
          switch (inp) {
            case 'host': this.setData({ hostAuto: res.data }); break;
            case 'record': this.setData({ recordAuto: res.data }); break;
            case 'absent': this.setData({ absentAuto: res.data }); break;
          }
        }
      });
    } else switch (inp) {
      case 'host': this.setData({ hostAuto: [] }); break;
      case 'record': this.setData({ recordAuto: [] }); break;
      case 'absent': this.setData({ absentAuto: [] }); break;
    }
  },
  // 设置自动填充姓名
  setMan(name, inp) {
    switch (inp) {
      case 'host': this.setData({ hostman: name, hostAuto: [] }); break;
      case 'record': this.setData({ recordman: name, recordAuto: [] }); break;
    }
  },
  // 清空自动填充列表
  clearAuto() { this.setData({ hostAuto: [], recordAuto: [], attendAuto: [], absentAuto: [] }); },
  // 图片选择添加
  addImage() {
    var _this = this;
    wx.chooseImage({
      count: 1, // 默认9  
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有  
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有  
      success: (fileRes) => {
        var tempFilePath = fileRes.tempFilePaths[0];
        Utils.UpLoad(`${API.UpLoad}`, tempFilePath, 'file', {}, (res) => {
          if (res.statusCode === 200) {
            let tempData = JSON.parse(res.data),
              tempNames = _this.data.imgNames,
              tempArr = _this.data.imgList;
            tempNames.push(tempData['fileName']);
            tempArr.push(tempFilePath);
            _this.setData({ imgList: tempArr });
            _this.setData({ imgNames: tempNames });
            wx.showToast({ title: '上传成功！', icon: 'success', duration: 2000 });
          } else wx.showToast({ title: '上传失败！', icon: 'error', duration: 2000 });
        });
      }
    })
  },
  // 数据提交
  submitEvent(ev) {
    let tmp = ev.detail.value;
    if (tmp.title !== '') {
      if (this.data.imgNames.length !== 0) {
        if (tmp.content !== '') {
          let absentList = this.data.absentList;
          tmp.imgs = this.data.imgNames.join(',');
          tmp.mid = this.data.mId;
          tmp.dept = wx.getStorageSync('dept');
          tmp.absent = '';
          for (let i = 0, len = absentList.length; i < len; i++) { if (absentList[i].name !== '') { tmp.absent += absentList[i].name + ':::' + absentList[i].reason + ';;;'; } }
          if (this.data.isClick) {
            this.setData({ isClick: false });
            Utils.Request(`${API.ActiveAdd}`, tmp, 'POST').then((res) => {
              if (res.statusCode === 200 ) {
                if(res.data.success){
                  wx.showToast({
                    title: '提交成功！', icon: 'none', duration: 2000, success: () => {
                      setTimeout(() => { wx.switchTab({ url: '../active' }); }, 1000);
                    }
                  });
                } else {
                  this.setData({ isClick: true });
                  wx.showToast({ title: '提交失败，请稍后再试！', icon: 'none', duration: 2000 });
                }
              } else {
                this.setData({ isClick: true });
                wx.showToast({ title: '服务器繁忙，请稍后再试！', icon: 'none', duration: 2000 });
              }
            });
          }
        } else wx.showToast({ title: '请输入内容！', icon: 'none', duration: 2000 });
      } else wx.showToast({ title: '至少上传1张图片！', icon: 'none', duration: 2000 });
    } else wx.showToast({ title: '请输入标题！', icon: 'none', duration: 2000 });
  },
  // 设置日期
  setDate(ev) { this.setData({ date: ev.detail.value }); },
  // 获取当前日期
  getDate() {
    let date = new Date(),
      Y = date.getFullYear(),
      M = date.getMonth() + 1,
      D = date.getDate();
    M = M < 10 ? `0${M}` : M;
    D = D < 10 ? `0${D}` : D;
    this.setData({ date: `${Y}-${M}-${D}` });
  }
}
Page(addConfig);