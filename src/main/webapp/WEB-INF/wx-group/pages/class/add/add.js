const API = require("../../../config/api.js");
const Utils = require("../../../utils/utils.js");
const addConfig = {
  data: {
    mId: '',
    date: '',
    hostman: '',
    recordman: '',
    hostAuto: [],
    recordAuto: [],
    player:'',
    playerAuto:[],
    imgList: [],
    imgNames: [],
    isClick: true
  },
  onLoad() {
    let date = new Date(),
      Y = date.getFullYear(),
      M = date.getMonth() + 1,
      D = date.getDate(); 
    M = M < 10 ? `0${M}` : M;
    D = D < 10 ? `0${D}` : D;
    this.setData({ date: `${Y}-${M}-${D}` });
  },
  // 主持人输入
  hostmanInp(ev) { this.manInp(ev.detail.value, 'host'); },
  // 自动填充设置主持人
  setHostman(ev) { this.setMan(ev.currentTarget.dataset.name, 'host'); },
  // 记录人输入
  recordmanInp(ev) { this.manInp(ev.detail.value, 'record'); },
  // 自动填充设置记录人
  setRecordman(ev) { this.setMan(ev.currentTarget.dataset.name, 'record'); },
  // 播放员输入
  playerInp(ev) { this.manInp(ev.detail.value, 'player'); },
  // 自动填充设置播放员
  setPlayer(ev) { this.setMan(ev.currentTarget.dataset.name, 'player'); },
  // 自动填充姓名输入
  manInp(val, inp) {
    if (val !== '') {
      Utils.Request(`${API.AutoFill}`, { name: val, dept: wx.getStorageSync('dept') }).then((res) => {
        if (res.statusCode === 200) {
          switch (inp) {
            case 'host': this.setData({ hostAuto: res.data }); break;
            case 'record': this.setData({ recordAuto: res.data }); break;
            case 'player': this.setData({ playerAuto: res.data }); break;
          }
        }
      });
    } else switch (inp) {
      case 'host': this.setData({ hostAuto: [] }); break;
      case 'record': this.setData({ recordAuto: [] }); break;
      case 'player': this.setData({ playerAuto: [] }); break;
    }
  },
  // 设置自动填充姓名
  setMan(name, inp) {
    switch (inp) {
      case 'host': this.setData({ hostman: name, hostAuto: [] }); break;
      case 'record': this.setData({ recordman: name, recordAuto: [] }); break;
      case 'player': this.setData({ player: name, playerAuto:[] }); break;
    }
  },
  // 清空自动填充列表
  clearAuto() { this.setData({ hostAuto: [], recordAuto: [], attendAuto: [], absentAuto: [] }); },
  // 图片选择添加
  addImage() {
    var _this = this;
    wx.chooseImage({
      count: 1, // 默认9  
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有  
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有  
      success: (fileRes) => {
        var tempFilePath = fileRes.tempFilePaths[0];
        Utils.UpLoad(`${API.UpLoad}`, tempFilePath, 'file', {}, (res) => {
          if (res.statusCode === 200) {
            let tempData = JSON.parse(res.data),
              tempNames = _this.data.imgNames,
              tempArr = _this.data.imgList;
            tempNames.push(tempData['fileName']);
            tempArr.push(tempFilePath);
            _this.setData({ imgList: tempArr });
            _this.setData({ imgNames: tempNames });
            wx.showToast({ title: '上传成功！', icon: 'success', duration: 1000 });
          } else wx.showToast({ title: '上传失败！', icon: 'error', duration: 1000 });
        });
      }
    })
  },
  // 数据提交
  submitEvent(ev) {
    let tmp = ev.detail.value;
    if (tmp.title !== '') {
      if (this.data.imgNames.length >1) {
        if (tmp.content !== '') {
          tmp.imgs = this.data.imgNames.join(',');
          tmp.dept = wx.getStorageSync('dept');
          if (this.data.isClick) {
            this.setData({ isClick: false });
            Utils.Request(`${API.ClassAdd}`, tmp, 'POST').then((res) => {
              if (res.statusCode === 200) {
                if (res.data.success){
                  wx.showToast({
                    title: '提交成功！', icon: 'none', duration: 2000, success: () => {
                      setTimeout(() => { wx.switchTab({ url: '../class' }); }, 1000);
                    }
                  });
                } else {
                  this.setData({ isClick: true });
                  wx.showToast({ title: '提交失败，请稍后再试！', icon: 'none', duration: 2000 });
                }
              } else {
                this.setData({ isClick: true });
                wx.showToast({ title: '服务器繁忙，请稍后再试！', icon: 'none', duration: 2000 });
              }
            });
          }
        } else wx.showToast({ title: '请输入内容！', icon: 'none', duration: 2000 });
      } else wx.showToast({ title: '至少上传2张(不同角度)图片！', icon: 'none', duration: 2000 });
    } else wx.showToast({ title: '请输入标题！', icon: 'none', duration: 2000 });
  },
  // 设置日期
  setDate(ev) { this.setData({ date: ev.detail.value }); }
}
Page(addConfig);