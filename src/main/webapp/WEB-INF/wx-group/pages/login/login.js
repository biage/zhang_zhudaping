const API = require("../../config/api.js");
const Utils = require("../../utils/utils.js");
const loginConfig = {
  data:{},
  login(ev){
    let form = ev.detail.value;
    if (form.account !== '') {
      if (form.password !== '') {
        Utils.Request(`${API.Login}`, form, "POST").then((res) => {
          if (res.statusCode===200){
            res = res.data;
            if (res.success){
              wx.setStorageSync('dept', res.data.dept);
              wx.showToast({
                title: '登录成功！',
                icon: 'success',
                duration: 2000,
                success: () => {
                  setTimeout(() => {
                    wx.switchTab({ url: '../index/index' });
                  }, 1000);
                }
              });
            }else wx.showToast({ title: res.message, icon: 'none', duration: 2000 });
          }
        });
      }else wx.showToast({ title: '请输入登录密码！', icon: 'none', duration: 2000 });
    }else wx.showToast({ title: '请输入用户账号！', icon: 'none', duration: 2000 });
  }
}
Page(loginConfig);