const Root = 'https://zzdj.yxzhdj.com';/* 所有数据接口根地址 */
// const Root = '';/* 所有数据接口根地址 */
// const Root = 'http://localhost:8088';
module.exports = {
  Login:`${Root}/doLogin`,/*登录*/
  IndexList: `${Root}/meeting/list?type=1`,/*三会一课列表页*/
  ActiveList: `${Root}/meeting/list?type=2`,/*支部活动列表页*/
  ClassList: `${Root}/meeting/list?type=3`,/*远程教育列表页*/
  IndexAdd: `${Root}/meeting/mobilSave?type=1`,/*三会一课提交*/
  ActiveAdd: `${Root}/meeting/mobilSave?type=2`,/*支部活动提交*/
  ClassAdd: `${Root}/meeting/mobilSave?type=3`,/*远程教育提交*/
  Details: `${Root}/meeting/details`,/*详情页接口*/
  GetMID: `${Root}/meeting/getMeetingId`,/*获取会议标识*/
  GetMType: `${Root}/dp/api/shykClass`,/*获取会议类型*/
  AutoFill: `${Root}/dp/api/Automenber`,
  ImgBase:`${Root}/upload/`,/*图片路径前缀*/
  UpLoad:`${Root}/blog/upload`,/*图片上传*/
}