function Requset(URL) {
  let data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  let method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'GET';
  let header = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' };
  return new Promise(function (resolve, reject) {
    wx.request({
      url: URL, data: data, method: method, header: header,
      success: function (res) { resolve(res) },
      fail: function (res) { reject(res) }
    });
  });
}

function UpLoad(URL, FilePath, Name, FormData, CallBack) {
  wx.uploadFile({
    url: URL,
    filePath: FilePath,
    name: Name,
    header: { 'content-type': 'multipart/form-data' },
    formData: FormData,
    success: (res) => {
      if (res.statusCode === 200) {
        return typeof CallBack == "function" && CallBack(res);
      } else {
        return typeof CallBack == "function" && CallBack(false);
      }
    }, fail: () => {
      return typeof CallBack == "function" && CallBack(false);
    }
  });
}
/**
  * 时间戳格式化函数
  * @param fmt    格式化模板(例："YYYY-MM-DD hh:mm:ss")
  * @param time   时间戳(10位时需要*1000,13位则不需要,若不传则为当前时间)
  * @returns {*}  返回格式化完成的时间格式
  */
function timeFormat(fmt, time) {
  time = time && (time.toString().length !== 10 ? time : time * 1000);
  var _that = time ? new Date(time) : new Date(),
    o = {
      "M+": _that.getMonth() + 1,                 //月份
      "D+": _that.getDate(),                    //日
      "h+": _that.getHours(),                   //小时
      "m+": _that.getMinutes(),                 //分
      "s+": _that.getSeconds(),                 //秒
      "q+": Math.floor((_that.getMonth() + 3) / 3), //季度
      "S": _that.getMilliseconds()             //毫秒
    };
  if (/(Y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (_that.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  }
  return fmt;
};
module.exports = {
  Request: Requset,
  UpLoad: UpLoad,
  timeFormat:timeFormat
}
