$(function(){
	var login_value = getCookie('login_cookie');
	if(login_value == 'yes'){
		var username = getCookie('username');
		$('.box-user span').text(username);
	}
	$('.box-user').on('click',function(){
		// window.location.href = 'login.html';
		$('.common-login').load('dlan.html')
	})
	$('.box-link-item').on('click',function(){
		var imgsrc = $(this).find('img').attr('src');
		var htmlsrc = imgsrc.split('.')[0];
		htmlsrc = htmlsrc.split('/');
		var htmlsrcLen = htmlsrc.length;
		htmlsrc = htmlsrc[htmlsrcLen - 1];
		if(htmlsrc == 'jdgk'){
			window.location.href = 'index.html';
		}else if(htmlsrc == 'shyk' || htmlsrc == 'zbtz' || htmlsrc == 'dymc'){
			if(login_value == 'yes'){
				window.location.href = htmlsrc + '.html';
			}else{
				$('.common-login').load('dlan.html');
			}
		}else{
			window.location.href = htmlsrc + '.html';
		}
	})
	$.ajax({
		type:"post",
		url:API.counts,
		async:true,
		success:function(data){
			var html = '<div class="box-cont-yuan-item">';
				html +=	'<p class="b-c-y-i-num">'+data.center+'个</p>';
				html +=		'<p class="b-c-y-i-text">党建工作分站</p>';
				html +=	'</div>';
				html +=	'<div class="box-cont-yuan-item">';
				html +=		'<p class="b-c-y-i-num">'+data.branch+'个</p>';
				html +=		'<p class="b-c-y-i-text">党(总)支部</p>';
				html +=	'</div>';
				html +=	'<div class="box-cont-yuan-item">';
				html +=		'<p class="b-c-y-i-num">'+data.member+'个</p>';
				html +=		'<p class="b-c-y-i-text">党员</p>';
				html +=	'</div>';
			$('.box-cont-yuan').html(html);
			$('.box-cont-yuan-item').on('click',function(){
				if($(this).index() == 2){
					window.location.href = 'dymc.html';
				}else{
					window.location.href = 'zzgj.html';
				}
			})
		}
	});
	$('.box-back').on('click',function(){
		window.location.href = 'index.html';
	})
	
})
function setCookie(name,value) 
{ 
    var hour = 1; 
    var exp = new Date(); 
    exp.setTime(exp.getTime() + hour*60*60*1000); 
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString(); 
} 
function getCookie(name) 
{ 
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
 
    if(arr=document.cookie.match(reg))
 
        return unescape(arr[2]); 
    else 
        return null; 
} 
function delCookie(name) 
{ 
    var exp = new Date(); 
    exp.setTime(exp.getTime() - 1); 
    var cval=getCookie(name); 
    if(cval!=null) 
    document.cookie= name + "="+cval+";expires="+exp.toGMTString(); 
} 
function Null(data){
	if(data){
		return data;
	}else{
		return '';
	}
}