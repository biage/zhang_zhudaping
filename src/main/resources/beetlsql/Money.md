list
===
SELECT  CONCAT(',',IFNULL(b.`d_id`,''),',',IFNULL(pb.`d_id`,''),',',IFNULL(ppb.d_id,''),',') dept,b.d_id,i.name,m.* FROM money m
LEFT JOIN info i ON i.id  = m.dy_id
LEFT JOIN branch b ON i.`d_id` = b.`id`
LEFT JOIN branch pb ON b.`p_id` = pb.`id`
LEFT JOIN branch ppb ON pb.`p_id` = ppb.`id`



ismoney
===
SELECT m.*,i.name name FROM money m
 left join info i on i.id = m.dy_id
 WHERE m.year = #{year}
 AND m.`dy_id` IN (
 	SELECT id FROM info WHERE d_id = #{dept}
 )
limit #{page},#{rows}

count
===
SELECT count(1) FROM info WHERE d_id = #{dept}

export
===
SELECT i.name, i.`d_id`,m.* FROM money m
LEFT JOIN info i ON i.`id` = m.`dy_id`
where i.d_id = #{bid}
and m.year = #{year}