list
===
SELECT i.*,b.name dname,fz.name fzname,b.p_id pid FROM info i
LEFT JOIN branch b ON b.id = i.d_id
left join branch fz on b.p_id = fz.id
where 1 = 1
@if(!isEmpty(d_id)){
    and i.d_id = #{d_id}
@}
@if(!isEmpty(pid)){
    and b.p_id = #{pid}
@}
@if(!isEmpty(name)){
    and i.name like #{name}
@}

one
===
SELECT i.*,b.name dname FROM info i
LEFT JOIN branch b ON b.id = i.d_id
where i.id = #{id}

centers
===
SELECT b.id,b.name FROM  branch b
LEFT JOIN branch ub ON ub.`p_id` = b.`id`
WHERE b.p_id = 0
@if(!isEmpty(branch)){
    and ub.id = #{branch}
@}
group by b.name
order by b.id asc

branchs
===
SELECT id,name FROM  branch
where p_id = #{pid}
@if(!isEmpty(branch)){
    and id = #{branch}
@}

members
===
SELECT i.*,YEAR( FROM_DAYS( DATEDIFF( NOW( ), i.birthday))) howold,b.name dname FROM info i
LEFT JOIN branch b ON b.id = i.d_id
where i.d_id = #{bid}

countedu
===
SELECT IFNULL(edu,"") `name`,COUNT(0) num FROM info GROUP BY edu
ORDER BY temp asc

countyear
===
SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(birthday)), '%Y')+0 AS name,COUNT(0) as value FROM info GROUP BY name


countmap
===
SELECT IFNULL(CONCAT(location_x,",",location_y),"") location,NAME,id
FROM branch
 where 1 = 1
  @if(!isEmpty(pid)){
        and p_id = #{pid}
        @}
      and CONCAT(location_x,location_y) <> ''


ismoney
===
SELECT i.name,IFNULL(i.ismoney,0) ismoney FROM info i


plan
===
SELECT plan FROM plan
where b_id = #{id}
ORDER BY yearmonth DESC
LIMIT 0,1


lunbo
===
SELECT a.`URL`,l.`n_id` FROM lunbo l
LEFT JOIN tfw_attach a ON a.`ID` = l.`img`
ORDER BY l.`num` ASC

shykimg
===
SELECT a.`URL`,l.`n_id` FROM shykimg l
LEFT JOIN tfw_attach a ON a.`ID` = l.`img`

branchcount
===
select count(1) from branch where p_id > 0

membercount
===
select count(1) from info

membersByName
===
SELECT i.id,i.name FROM info i
LEFT JOIN  branch b ON i.`d_id` = b.id
LEFT JOIN branch bb ON b.`p_id` = bb.`id`
WHERE i.name LIKE #{name}
AND CONCAT(',',IFNULL(b.`id`,''),',',IFNULL(bb.`id`,''),',',IFNULL(bb.p_id,''),',')  LIKE  #{dept}
LIMIT 0,20


membersByBid
===
SELECT id,name FROM info 
where d_id = #{bid}

zzgzzd
===
select * from info 
where type = 0


zzgzzd
===
SELECT * FROM zdjh 
WHERE TYPE = 0
ORDER BY yearmonth DESC
LIMIT 0,1

zzgzjh
===
SELECT z.*,d.name cartname,yearmonth ym FROM zdjh z
LEFT JOIN (SELECT num,NAME FROM tfw_dict WHERE CODE=109)d ON z.cart=d.num
WHERE yearmonth = (
	SELECT yearmonth FROM zdjh WHERE TYPE = 1
	GROUP BY yearmonth
	ORDER BY yearmonth DESC
	LIMIT 0,1
)
AND z.type = 1
ORDER BY z.id ASC

record
===
SELECT i.`name`,i.id uid,IFNULL(s.m1,0)+IFNULL(s.m2,0)+IFNULL(s.m3,0) q1, 
IFNULL(m4,0)+IFNULL(m5,0)+IFNULL(m6,0) q2, 
IFNULL(m7,0)+IFNULL(m8,0)+IFNULL(m9,0) q3, 
IFNULL(m10,0)+IFNULL(m11,0)+IFNULL(m12,0) q4
FROM gdxxr_sign s
LEFT JOIN info i ON i.id = s.u_id
where 1 = 1
 @if(!isEmpty(quarter1)){
         and IFNULL(m1,0)+IFNULL(m2,0)+IFNULL(m3,0) = #{num}
    @}else if(!isEmpty(quarter2)){
         and IFNULL(m4,0)+IFNULL(m5,0)+IFNULL(m6,0) = #{num}
    @}else if(!isEmpty(quarter3)){
        and IFNULL(m7,0)+IFNULL(m8,0)+IFNULL(m9,0) = #{num}
  @}else if(!isEmpty(quarter4)){
        and IFNULL(m10,0)+IFNULL(m11,0)+IFNULL(m12,0) = #{num}
    @}
AND s.d_id = #{did}



reason
===
SELECT a.leave_reason  reason
FROM meeting_attend a
LEFT JOIN meeting m ON m.id = a.`m_id`
WHERE leave_user = #{uid}
AND LOCATE(DATE_FORMAT(m.`date`,'%m'), #{months}) > 0


expertMembers
===
SELECT i.*,YEAR( FROM_DAYS( DATEDIFF( NOW( ), i.birthday))) howold,b.name dname FROM info i
LEFT JOIN branch b ON b.id = i.d_id
LEFT JOIN branch bb ON b.`p_id` = bb.`id`
LEFT JOIN branch pbb ON bb.`p_id` = pbb.`id`
WHERE
CONCAT(',',IFNULL(b.`d_id`,''),',',IFNULL(b.`id`,''),',',IFNULL(bb.`d_id`,''),',',IFNULL(pbb.d_id,''),',')
LIKE  #{bid}


countInfo
===
SELECT COUNT(1) FROM info WHERE d_id = #{bid}

countHuodong
===
SELECT COUNT(1) FROM meeting WHERE d_id = #{bid}