list
===
SELECT CONCAT(",",IFNULL(b.d_id,""),",",IFNULL(pd.d_id,""),",",IFNULL(ppd.d_id,"")) did,w.* FROM ycjy_week w
LEFT JOIN branch b ON b.id = w.d_id
LEFT JOIN branch pd ON b.p_id = pd.id
LEFT JOIN branch ppd ON pd.p_id = ppd.id

weektz
===
select * from ycjy_week
where year = #{year}
and d_id = #{did}
group by num
order by num desc