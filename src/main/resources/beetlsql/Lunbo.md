list
===
SELECT a.`URL` url,n.`title`,l.* 
FROM lunbo l
LEFT JOIN news n ON l.`n_id` = n.`id`
LEFT JOIN tfw_attach a ON a.`ID` = n.`img`
ORDER BY l.num ASC