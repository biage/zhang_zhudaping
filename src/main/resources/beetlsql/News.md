list
===
SELECT n.*,a.url imgurl FROM news n
LEFT JOIN tfw_attach a ON n.img = a.id


one
===
SELECT n.*,a.url imgurl FROM news n
LEFT JOIN tfw_attach a ON n.img = a.id
where n.id = #{id}


bybranch
===
SELECT n.*,a.url imgurl FROM news n
LEFT JOIN tfw_attach a ON n.img = a.id
where n.d_id = #{did}

select
===
select id,title from news
order by id desc
limit 0,20


shyklist
===
SELECT m.d_id,m.id,DATE_FORMAT(m.date,'%y-%m-%d') DATE,IFNULL(m.imgs,'default.jpg') url,m.title
FROM meeting m
LEFT JOIN branch b ON m.`d_id` = b.`id`
LEFT JOIN branch pd ON b.`p_id` = pd.`id`
WHERE CONCAT(',',IFNULL(b.`id`,''),',',IFNULL(b.`p_id`,''),',',IFNULL(pd.p_id,''),',') LIKE #{dept}
and type in (1,2)
ORDER BY id DESC

shyklunbo
===
 SELECT m.id,IFNULL(m.imgs,'default.jpg') url
 FROM meeting m
 LEFT JOIN branch b ON m.`d_id` = b.`id`
 LEFT JOIN branch pd ON b.`p_id` = pd.`id`
 WHERE CONCAT(',',IFNULL(b.`id`,''),',',IFNULL(b.`p_id`,''),',',IFNULL(pd.p_id,''),',') LIKE #{dept}
 AND TYPE IN (1,2)
 ORDER BY id DESC
