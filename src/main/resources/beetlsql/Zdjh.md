list
===
SELECT z.*,d.name cartname,CONCAT(YEAR(z.yearmonth),"-",MONTH(z.`yearmonth`)) ym FROM zdjh z
LEFT JOIN (SELECT num,NAME FROM tfw_dict WHERE CODE=109)d ON z.cart=d.num
WHERE CONCAT(YEAR(z.yearmonth),MONTH(z.`yearmonth`)) = (
	SELECT CONCAT(YEAR(yearmonth),MONTH(`yearmonth`)) FROM zdjh WHERE TYPE = 1
	GROUP BY yearmonth
	ORDER BY yearmonth DESC
	LIMIT 0,1
)
AND z.type = 1
ORDER BY z.id ASC

year
===
select year from xytz
where type = #{type}
and d_id = #{did}
group by year

