list
===
select b.name zbname,IF(b.id = 1,b.name,pb.name) fzname,m.* from meeting m
left join branch b on b.id = m.d_id
LEFT JOIN branch pb ON pb.id = b.`p_id`
LEFT JOIN branch ppb ON ppb.`id` = pb.`p_id`
WHERE (m.d_id = #{bid} OR pb.`id` = #{bid} OR ppb.`id` = #{bid} OR ppb.`p_id` = #{bid})
AND m.TYPE = #{type}
order by id desc

one
===
select b.name,m.* from meeting m
left join branch b on b.id = m.d_id
where id = #{id}


getMeetings
===
SELECT id,DATE_FORMAT(DATE ,'%y-%m-%d') DATE FROM meeting
WHERE d_id = #{did}
AND TYPE = #{type}
GROUP BY id

getMembersn
===
SELECT id,NAME FROM info
WHERE d_id = #{did}


getMeetingById
===
SELECT m_id,leave_reason reason FROM meeting_attend
WHERE leave_user = #{uid}
AND TYPE = #{type}


nametoid
===
SELECT i.* FROM info i
LEFT JOIN branch d ON d.id = i.`d_id`
LEFT JOIN branch pd ON d.`p_id` = pd.`id`
WHERE i.name = #{name}
AND ( d.id = #{dept} OR d.`p_id` = #{dept} OR pd.`p_id` = #{dept})



