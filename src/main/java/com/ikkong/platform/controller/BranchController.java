package com.ikkong.platform.controller;

import java.util.List;
import java.util.Map;
import com.ikkong.core.base.BaseController;
import com.ikkong.core.dao.Blade;
import com.ikkong.core.jfinal.ext.kit.JsonKit;
import com.ikkong.core.toolbox.Record;
import com.ikkong.system.controller.base.UrlPermissController;
import com.ikkong.platform.model.Branch;
import com.ikkong.platform.service.BranchService;
import com.ikkong.platform.service.impl.BranchServiceImpl;
import com.ikkong.system.util.CommonUtil;

/**
 * Generated by Blade.
 * 2018-09-14 21:12:30
 */
public class BranchController extends UrlPermissController {
	private static String CODE = "branch";
	private static String PERFIX = "branch";
	private static String LIST_SOURCE = "Branch.list";
	private static String BASE_PATH = "/platform/branch/";
	
	BranchService service = new BranchServiceImpl();
	
	public void index() {
		setAttr("code", CODE);
		render(BASE_PATH + "branch.html");
	}

	public void add() {
		setAttr("code", CODE);
		render(BASE_PATH + "branch_add.html");
	}

	public void edit() {
		String id = getPara(0);
		Branch branch = service.findById(id);
		setAttr("model", JsonKit.toJson(branch));
		setAttr("id", id);
		setAttr("code", CODE);
		render(BASE_PATH + "branch_edit.html");
	}

	public void view() {
		String id = getPara(0);
		Branch branch = service.findById(id);
		setAttr("model", JsonKit.toJson(branch));
		setAttr("id", id);
		setAttr("code", CODE);
		render(BASE_PATH + "branch_view.html");
	}

	public void list() {
        List<Map> select = Blade.dao().select("Branch.list", Map.class, Record.create());
		renderJson(CommonUtil.fenye(select,getParaToInt("page",1),getParaToInt("rows",20)));
	}

	public void save() {
		Branch branch = mapping(PERFIX, Branch.class);
		boolean temp = service.save(branch);
		if (temp) {
			renderJson(success(SAVE_SUCCESS_MSG));
		} else {
			renderJson(error(SAVE_FAIL_MSG));
		}
	}

	public void update() {
		Branch branch = mapping(PERFIX, Branch.class);
		boolean temp = service.update(branch);
		if (temp) {
			renderJson(success(UPDATE_SUCCESS_MSG));
		} else {
			renderJson(error(UPDATE_FAIL_MSG));
		}
	}

	public void remove() {
		String ids = getPara("ids");
		int cnt = service.deleteByIds(ids);
		if (cnt > 0) {
			renderJson(success(DEL_SUCCESS_MSG));
		} else {
			renderJson(error(DEL_FAIL_MSG));
		}
	}
}
