package com.ikkong.platform.model;

import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;
import com.ikkong.core.annotation.BindID;
import com.ikkong.core.model.BaseModel;

import java.math.BigDecimal;

/**
 * Generated by Blade.
 * 2018-09-14 21:12:30
 */
@Table(name = "branch")
@BindID(name = "id")
@SuppressWarnings("serial")
public class Branch extends BaseModel {
    	private Integer id;
    	private Integer d_id; //deptid
    	private Integer isgood; //是否优秀党支部
    	private Integer num; //排序
    	private Integer p_id; //上级组织
    	private BigDecimal location_x; //经度
    	private BigDecimal location_y; //纬度
    	private String name; //组织名
    	private String position; //地址

	private String leader; //组织名
	private String leader_detail; //地址

    	@AutoID

		public String getLeader() {
			return leader;
		}

	public void setLeader(String leader) {
		this.leader = leader;
	}

	public String getLeader_detail() {
		return leader_detail;
	}

	public void setLeader_detail(String leader_detail) {
		this.leader_detail = leader_detail;
	}

    	public Integer getId() {
    		return id;
    	}

    	public void setId(Integer id) {
    		this.id = id;
    	}

    	public Integer getD_id() {
    		return d_id;
    	}

    	public void setD_id(Integer d_id) {
    		this.d_id = d_id;
    	}

    	public Integer getIsgood() {
    		return isgood;
    	}

    	public void setIsgood(Integer isgood) {
    		this.isgood = isgood;
    	}

    	public Integer getNum() {
    		return num;
    	}

    	public void setNum(Integer num) {
    		this.num = num;
    	}

    	public Integer getP_id() {
    		return p_id;
    	}

    	public void setP_id(Integer p_id) {
    		this.p_id = p_id;
    	}

    	public BigDecimal getLocation_x() {
    		return location_x;
    	}

    	public void setLocation_x(BigDecimal location_x) {
    		this.location_x = location_x;
    	}

    	public BigDecimal getLocation_y() {
    		return location_y;
    	}

    	public void setLocation_y(BigDecimal location_y) {
    		this.location_y = location_y;
    	}

    	public String getName() {
    		return name;
    	}

    	public void setName(String name) {
    		this.name = name;
    	}

    	public String getPosition() {
    		return position;
    	}

    	public void setPosition(String position) {
    		this.position = position;
    	}

}
