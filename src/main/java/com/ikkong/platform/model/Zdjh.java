package com.ikkong.platform.model;

import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;
import com.ikkong.core.annotation.BindID;
import com.ikkong.core.model.BaseModel;

import java.util.Date;

/**
 * Generated by Blade.
 * 2018-10-12 22:00:17
 */
@Table(name = "zdjh")
@BindID(name = "id")
@SuppressWarnings("serial")
public class Zdjh extends BaseModel {
    	private Integer id;
    	private Integer type; //类型-0工作制度-1工作已计划
    	private String content;
    	private String yearmonth;
	private Integer cart;

	@AutoID
	public Integer getCart() {
		return cart;
	}

	public void setCart(Integer cart) {
		this.cart = cart;
	}
    	public Integer getId() {
    		return id;
    	}

    	public void setId(Integer id) {
    		this.id = id;
    	}

    	public Integer getType() {
    		return type;
    	}

    	public void setType(Integer type) {
    		this.type = type;
    	}

    	public String getContent() {
    		return content;
    	}

    	public void setContent(String content) {
    		this.content = content;
    	}

    	public String getYearmonth() {
    		return yearmonth;
    	}

    	public void setYearmonth(String yearmonth) {
    		this.yearmonth = yearmonth;
    	}

}
