package com.ikkong.system.util;

import com.ikkong.core.dao.Blade;
import com.ikkong.core.jfinal.ext.shiro.ShiroKit;
import com.ikkong.platform.model.Branch;
import org.beetl.sql.core.kit.StringKit;

import java.io.File;
import java.util.*;


public class CommonUtil {

	public static void delFile(String path,String filename){
		File file=new File(path+"/"+filename);
		if(file.exists()&&file.isFile())
			file.delete();
	}

	public static void delFolder(String folderPath) {
		try {
			delAllFile(folderPath); //删除完里面所有内容
			String filePath = folderPath;
			filePath = filePath.toString();
			java.io.File myFilePath = new java.io.File(filePath);
			myFilePath.delete(); //删除空文件夹
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean delAllFile(String path) {
		boolean flag = false;
		File file = new File(path);
		if (!file.exists()) {
			return flag;
		}
		if (!file.isDirectory()) {
			return flag;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);//先删除文件夹里面的文件
				delFolder(path + "/" + tempList[i]);//再删除空文件夹
				flag = true;
			}
		}
		return flag;
	}



	public static String  getMyBranch(String deptId){
		if(StringKit.isBlank(deptId)) {
			return 0+"";
		}
			Branch firstBy = Blade.create(Branch.class).findFirstBy("d_id = " + deptId, Branch.class);
			if(firstBy!=null){
				deptId = firstBy.getId()+"";
				return deptId;
			}else {
				deptId  = 0+"";
				return deptId;
			}

	}

	public static String parseHtml(String html) {

        /*
         * <.*?>为正则表达式，其中的.表示任意字符，*?表示出现0次或0次以上，此方法可以去掉双头标签(双头针对于残缺的标签)
         * "<.*?"表示<尖括号后的所有字符，此方法可以去掉残缺的标签，及后面的内容
         * " "，若有多种此种字符，可用同一方法去除
         */
			html = html.replaceAll("<.*?>", "  ").replaceAll(" ", " ");
			html = html.replaceAll("<.*?", "");
			return (html);
	}
	public static Object fenye(List maps,int page,int rows){
		if(maps.size()<1){
			return null;
		}
		int start = (page-1)*rows;
		double d = maps.size()*1.00/rows*1.00;
		int end = start + rows;
		if (end>maps.size()){
			end = maps.size();
		}
		List<Map> newmap = maps.subList(start,end);
		Map sss = new HashMap();
		sss.put("rows",newmap);
		sss.put("total",Math.ceil(d));
		sss.put("page",page);
		sss.put("records",maps.size());
		sss.put("code",0);
		sss.put("statusCode",200);
		Object o = sss;
		return o;
	}

	public static void createFieldNameList(List<Map<String, Object>> fieldNameList, String index, String width, String textAlign, String onRenderEvent, boolean isHidden) {
		Map<String, Object> fieldNameMap = new HashMap<String, Object>();
		fieldNameMap.put("index", index);
		fieldNameMap.put("width", width);
		fieldNameMap.put("textAlign", textAlign);
		fieldNameMap.put("isHidden", isHidden);
		fieldNameMap.put("onRenderEvent", onRenderEvent);
		fieldNameMap.put("isSort", true);
		fieldNameList.add(fieldNameMap);
	}
	public static boolean ynadmin(){
        List<String> roleList = ShiroKit.getUser().getRoleList();
        boolean b = false;
        for(String ss:roleList){
            if("1".equals(ss) || "2".equals(ss)|| "8".equals(ss)|| "9".equals(ss)){
                b = true;
                break;
            }
        }
        return b;
    }

	public static boolean ynFenzhanAdmin(){
		List<String> roleList = ShiroKit.getUser().getRoleList();
		boolean b = false;
		for(String ss:roleList){
			if("1".equals(ss) || "2".equals(ss)|| "8".equals(ss)||"9".equals(ss)){
				b = true;
				break;
			}
		}
		return b;
	}

	public static boolean yncaiwu(){
		List<String> roleList = ShiroKit.getUser().getRoleList();
		boolean b = false;
		for(String ss:roleList){
			if("1".equals(ss) ||"2".equals(ss) ||"8".equals(ss) ||"9".equals(ss) ||"11".equals(ss) ||"12".equals(ss) ||"13".equals(ss)  ){
				b = true;
				break;
			}
		}
		return b;
	}
	public static boolean ynsuper(){
		List<String> roleList = ShiroKit.getUser().getRoleList();
		boolean b = false;
		for(String ss:roleList){
			if("1".equals(ss)){
				b = true;
				break;
			}
		}
		return b;
	}
	public static String quchong(String s){

		String[] array = s.split(",");
		Set<String> set = new HashSet<>();
		for(int i=0;i<array.length;i++){
			set.add(array[i]);
		}
		String[] arrayResult = (String[]) set.toArray(new String[set.size()]);
		return Arrays.toString(arrayResult);
	}
	public static void createEditFieldNameList(List<Map<String, Object>> fieldNameList, String index, String width, String textAlign, String onRenderEvent, boolean isHidden) {
		Map<String, Object> fieldNameMap = new HashMap<String, Object>();
		fieldNameMap.put("index", index);
		fieldNameMap.put("width", width);
		fieldNameMap.put("textAlign", textAlign);
		fieldNameMap.put("isHidden", isHidden);
		fieldNameMap.put("onRenderEvent", onRenderEvent);
		fieldNameMap.put("isSort", true);
		fieldNameMap.put("isEdit", true);
		fieldNameList.add(fieldNameMap);
	}

	public static void createFieldNameList(List<Map<String, Object>> fieldNameList, String index, String width, String textAlign, String onRenderEvent, boolean isHidden, boolean isSort) {
		Map<String, Object> fieldNameMap = new HashMap<String, Object>();
		fieldNameMap.put("index", index);
		fieldNameMap.put("width", width);
		fieldNameMap.put("textAlign", textAlign);
		fieldNameMap.put("isHidden", isHidden);
		fieldNameMap.put("onRenderEvent", onRenderEvent);
		fieldNameMap.put("isSort", isSort);
		fieldNameList.add(fieldNameMap);
	}

}
