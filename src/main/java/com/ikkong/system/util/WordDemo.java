package com.ikkong.system.util;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.rtf.RtfWriter2;
import org.beetl.sql.core.kit.StringKit;

/**
 * 创建word文档 步骤:
 * 1,建立文档
 * 2,创建一个书写器
 * 3,打开文档
 * 4,向文档中写入数据
 * 5,关闭文档
 */
public class WordDemo {

	public WordDemo() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<String>();
		names.add("名字");
		names.add("名字1");
		names.add("名字2");
		names.add("名字333");
		names.add("名字4");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");
		names.add("名字");






		// 创建word文档,并设置纸张的大小
		Document document = new Document(PageSize.A4);
		try {
			RtfWriter2.getInstance(document,
					new FileOutputStream("F:/word.doc"));

			document.open();

			//设置合同头

			Paragraph ph = new Paragraph();
			Font f  = new Font();

			Paragraph p = new Paragraph("第____季度党员参学统计",
					new Font(Font.NORMAL, 22, Font.BOLD, new Color(0, 0, 0)) );
			Paragraph p1 = new Paragraph("（此表每季度进行公式）",
					new Font(Font.NORMAL, 16, Font.NORMAL, new Color(0, 0, 0)) );
			Paragraph p2 = new Paragraph("全勤人员");
					new Font(Font.NORMAL, 16, Font.NORMAL, new Color(0, 0, 0)) ;
//			p.setAlignment(1);

			p.setAlignment(Element.ALIGN_CENTER);
			p1.setAlignment(Element.ALIGN_CENTER);
			p2.setAlignment(Element.ALIGN_LEFT);
			document.add(p);
			document.add(p1);
			document.add(p2);
			ph.setFont(f);

			// 设置中文字体
//			 BaseFont bfFont =
//			 BaseFont.createFont("STSongStd-Light",
//			"UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
			// Font chinaFont = new Font();
			/*
			 * 创建有三列的表格
			 */
			Table table = new Table(10,22);
			table.setWidth(100);

//			document.add(new Paragraph("生成表格"));
			table.setBorderWidth(1);



			table.setBorderColor(Color.BLACK);
			table.setPadding(10);
			table.setSpacing(0);

			for(String name:names){
			    Paragraph phh = new Paragraph("               " + name + "              ");
			    phh.setAlignment(Element.ALIGN_CENTER);
			    Cell cell = new Cell(phh);
				table.addCell(cell);
			}
			int i = names.size();
			for(int j = 0;j<220-i;j++){
                Paragraph phh = new Paragraph("               " +   "              ");
                phh.setAlignment(Element.ALIGN_CENTER);
                Cell cell = new Cell(phh);
                table.addCell(cell);
            }

			/*
			 * 添加表头的元素
			 */
//			Cell cell = new Cell("时间");//单元格
//			cell.setHeader(true);
////			cell.setColspan(4);//设置表格为三列
////			cell.setRowspan(4);//设置表格为三行
//			table.addCell(cell);
//			table.addCell("2018-12-11");
//			table.addCell("地点");
//			table.addCell("地点DEMO");
//			table.addCell("地点");
//			table.addCell("地点DEMO");
//			table.addCell("地点");
//			table.addCell("地点DEMO");
//			table.addCell("地点");
//			table.addCell("地点DEMO");
//			table.addCell("地点");
//			table.addCell("地点DEMO");
//
//
//
//			table.endHeaders();// 表头结束
//
//			// 表格的主体
//			cell = new Cell("Example cell 2");
//			cell.setRowspan(2);//当前单元格占两行,纵向跨度
//			table.addCell(cell);
//			table.addCell("1,1");
//			table.addCell("1,2");
//			table.addCell("1,3");
//			table.addCell("1,4");
//			table.addCell("1,5");
//			table.addCell(new Paragraph("用java生成的表格1"));
//			table.addCell(new Paragraph("用java生成的表格2"));
//			table.addCell(new Paragraph("用java生成的表格3"));
//			table.addCell(new Paragraph("用java生成的表格4"));
//			document.add(new Paragraph("用java生成word文件"));
			document.add(table);
			document.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



    public static boolean quanqin(List<Map> names, String jidu, String path) {
        // 创建word文档,并设置纸张的大小
        Document document = new Document(PageSize.A4);
        try {
            RtfWriter2.getInstance(document,
                    new FileOutputStream(path));

            document.open();

            //设置合同头

            Paragraph ph = new Paragraph();
            Font f  = new Font();

            Paragraph p = new Paragraph("第"+jidu+"季度党员参学统计",
                    new Font(Font.NORMAL, 22, Font.BOLD, new Color(0, 0, 0)) );
            Paragraph p1 = new Paragraph("（此表每季度进行公示）",
                    new Font(Font.NORMAL, 16, Font.NORMAL, new Color(0, 0, 0)) );
            Paragraph p2 = new Paragraph("全勤人员");
            new Font(Font.NORMAL, 16, Font.NORMAL, new Color(0, 0, 0)) ;
//			p.setAlignment(1);

            p.setAlignment(Element.ALIGN_CENTER);
            p1.setAlignment(Element.ALIGN_CENTER);
            p2.setAlignment(Element.ALIGN_LEFT);
            document.add(p);
            document.add(p1);
            document.add(p2);
            ph.setFont(f);

            // 设置中文字体
//			 BaseFont bfFont =
//			 BaseFont.createFont("STSongStd-Light",
//			"UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
            // Font chinaFont = new Font();
            /*
             * 创建有三列的表格
             */
            Table table = new Table(10,22);
            table.setWidth(100);

//			document.add(new Paragraph("生成表格"));
            table.setBorderWidth(1);



            table.setBorderColor(Color.BLACK);
            table.setPadding(10);
            table.setSpacing(0);

            for(Map name:names){
                Paragraph phh = new Paragraph("               " + name.get("name") + "              ");
                phh.setAlignment(Element.ALIGN_CENTER);
                Cell cell = new Cell(phh);
                table.addCell(cell);
            }
            int i = names.size();
            for(int j = 0;j<220-i;j++){
                Paragraph phh = new Paragraph("               " +   "              ");
                phh.setAlignment(Element.ALIGN_CENTER);
                Cell cell = new Cell(phh);
                table.addCell(cell);
            }

            document.add(table);
            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }


    public static boolean quexi(List<Map> names, String jidu, String path) {
        // 创建word文档,并设置纸张的大小
        Document document = new Document(PageSize.A4);
        try {
            RtfWriter2.getInstance(document,
                    new FileOutputStream(path));

            document.open();

            //设置合同头

            Paragraph ph = new Paragraph();
            Font f  = new Font();

            Paragraph p = new Paragraph("第"+jidu+"季度党员参学统计",
                    new Font(Font.NORMAL, 22, Font.BOLD, new Color(0, 0, 0)) );
            Paragraph p1 = new Paragraph("（此表每季度进行公示）",
                    new Font(Font.NORMAL, 16, Font.NORMAL, new Color(0, 0, 0)) );
            Paragraph p2 = new Paragraph("全勤人员");
            new Font(Font.NORMAL, 16, Font.NORMAL, new Color(0, 0, 0)) ;
//			p.setAlignment(1);

            p.setAlignment(Element.ALIGN_CENTER);
            p1.setAlignment(Element.ALIGN_CENTER);
            p2.setAlignment(Element.ALIGN_LEFT);
            document.add(p);
            document.add(p1);
            document.add(p2);
            ph.setFont(f);

            // 设置中文字体
//			 BaseFont bfFont =
//			 BaseFont.createFont("STSongStd-Light",
//			"UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
            // Font chinaFont = new Font();
            /*
             * 创建有三列的表格
             */
            Table table = new Table(6,22);
            table.setWidth(100);

//			document.add(new Paragraph("生成表格"));
            table.setBorderWidth(1);



            table.setBorderColor(Color.BLACK);
            table.setPadding(10);
            table.setSpacing(0);

            for(Map name:names){
                String r =StringKit.isBlank((String)name.get("reason"))?"无故缺席":name.get("reason").toString();
                Paragraph phh = new Paragraph("               " + name.get("name") + "              ");
                phh.setAlignment(Element.ALIGN_CENTER);
                Paragraph reason = new Paragraph("               " + r + "              ");
                reason.setAlignment(Element.ALIGN_CENTER);
                Cell cell = new Cell(phh);
                Cell cell2 = new Cell(reason);
                table.addCell(cell);
                table.addCell(cell2);
            }
            int i = names.size();
            for(int j = 0;j<220-i*2;j++){
                Paragraph phh = new Paragraph("               " +   "              ");
                phh.setAlignment(Element.ALIGN_CENTER);
                Cell cell = new Cell(phh);
                table.addCell(cell);
            }

            document.add(table);
            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}